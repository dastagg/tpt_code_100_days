# Talk Python Training - 100 Days of Code in Python

## 100 Days of Code - Python

This is my code for the [Talk Python Training - 100 Days of Code](https://training.talkpython.fm/courses/explore_100days_in_python/100-days-of-code-in-python) program.


Since the [GitHub repository](https://github.com/talkpython/100daysofcode-with-python-course) has a MIT licence on it, I will put the same licence on this.


This code will mostly follow along with the class but I may skip somethings or I may go off on a tangent.

This tutorial will only have one virtualenv created for it using 'pyenv'.
It is called D100-TPT-Code. 
Some of the days will want to create a unique venv and install packages.
I will not do that with this tutorial but instead try to maintain the requirements.txt file at the root of the tutorial and install from there.

